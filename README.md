# Short URL 插件

### 插件解析

```json
{
    "Info": {
        "Name": "插件名称",
        "Author": "插件作者",
        "Version": "插件版本",
        "Date": "最新编译日期",
        "Update": "该插件更新地址"
    },
    "Key": {
        "Deploy": "是否需要密匙",
        "URL": "密匙注册地址"
    },
    "Request": {
        "URL": "提交地址",
        "Method": "访问类型",
        "Parameter": "提交数据"
    },
    "Response": {
        "Type": "返回类型",
        "Parameter": "参数处理",
        "Prefix": "网址前缀"
    }
}
```


##### Info 插件信息
 - Name     插件名称
[注]:仅提供视觉效果
 - Author     插件作者
[注]:仅提供视觉效果
 - Version    版本号
[注]:仅提供视觉效果
 - Date         编译日期
[注]:仅提供视觉效果
 - Update    更新地址
[注]:单独更新时使用该地址进行更新

##### Key 插件密匙
 - Deploy 密匙配置
[true]:需要密匙
[false]:不需要密匙
 - URL 密匙注册地址
[注]:密匙为空时默认打开的注册地址

##### Request 请求数据
 - URL 请求地址
 - Method 请求类型
[0]:GET 访问
[1]:POST 提交
 - Parameter 请求参数
[注]:使用任意方式都将数据填写在此处,为 $GET$ 方式时自动拼接数据
[#Key#]:密匙变量
[$Url$]:网址变量

##### Response 响应数据

 - Type 处理方式
[Text]:直接输出显示
[Json]:$Parameter$ 处写E2EE键值表
[Regular]:$Parameter$ 处填写正则语法
 - Parameter 处理参数
[注]:根据 $Type$ 填写的值填写相应的参数/语法
 - Prefix 前缀
[注]:不为空则增加此项文本到数据前